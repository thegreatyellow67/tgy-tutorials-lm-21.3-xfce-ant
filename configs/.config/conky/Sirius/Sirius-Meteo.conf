conky.config = {
--==============================================================================

--  This theme is for conky version 1.10.8 or newer
--
--  SIRIUS2 - METEO
--  ( A part of Leonis Conky themes pack )
--
--  Original author : Closebox73
--  Created         : 2022/07/12
--  Variant         : Playerctl & Celsius
--  Modified by     : TheGreatYellow67
--  Modified date   : 2024/04/23
--  Variant         : Italian language
--
--  License         : Distributed under the terms of GPLv3
--  Notes           : Created on 1920x1080 Monitor

--==============================================================================

-- Size and Position settings --
  alignment = 'top_right',
  gap_x = 30,
  gap_y = 590,
  maximum_width = 200,
  minimum_height = 200,
  minimum_width = 200,
  
-- Text settings --
  use_xft = true,
  override_utf8_locale = true,
  font = 'Roboto:light:size=9',
  
-- Color Settings --
  default_color = '#f9f9f9',
  default_outline_color = 'white',
  default_shade_color = 'white',
  color1 = 'FF5D73',
  
-- Window Settings --
  background = false,
  draw_blended = false,
  border_width = 1,
  draw_borders = false,
  draw_graph_borders = false,
  draw_outline = false,
  draw_shades = false,
  own_window = true,
  own_window_class = 'Conky',
  stippled_borders = 0,

-- To get REAL TRANSPARENCY --
  own_window_type = 'desktop',
  own_window_transparent = false,
  own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager,below',
  own_window_colour = '000000',
  own_window_argb_visual = true,
  own_window_argb_value = 0,
  
-- Others --
  cpu_avg_samples = 2,
  net_avg_samples = 2,
  double_buffer = true,
  out_to_console = false,
  out_to_stderr = false,
  extra_newline = false,
  update_interval = 1,
  uppercase = false,
  use_spacer = 'none',
  show_graph_scale = false,
  show_graph_range = false,
}

conky.text = [[
##
## Script per il meteo ##
##
${execi 300 ~/.config/conky/Sirius/scripts/weather-v2.0.sh}\
##
## Immagini ##
##
${image ~/.config/conky/Sirius/res/bg-meteo.png -s 200x200 -p 0,0}\
${image ~/.config/conky/Sirius/res/line.png -s 180x1 -p 10,54}\
##
## Etichetta METEO
##
${offset 10}${voffset 8}${color1}${font Finlandica:bold:size=12}METEO DI OGGI${color}
##
## METEO
##
${offset 10}${voffset 0}${font Finlandica:bold:size=11}Località: ${font Finlandica:size=11}${execi 100 cat ~/.cache/weather.json | jq -r '.name'} (${execi 100 cat ~/.cache/weather.json | jq -r '.sys.country'})
${offset 10}${voffset 14}${font Finlandica:bold:size=11}Condizioni meteo attuali:
${offset 10}${font Finlandica:size=11}${execi 100 ~/.config/conky/Sirius/scripts/weather-description.sh}
${offset 10}${voffset 2}${font Easy Weather Icons:size=40}${execi 15 ~/.config/conky/Sirius/scripts/weather-text-icon.sh}${font}
${alignr 35}${voffset -48}${font Finlandica:size=25}${execi 100 cat ~/.cache/weather.json | jq '.main.temp' | awk '{print int($1+0.5)}'}°C
${offset 10}${voffset -10}${font Finlandica:bold:size=11}Velocità vento:${font Finlandica:size=10}${alignr 20}${execi 100 (cat ~/.cache/weather.json | jq '.wind.speed')} m/s
${offset 10}${voffset 0}${font Finlandica:bold:size=11}Umidità:${font Finlandica:size=10}${alignr 20}${execi 100 (cat ~/.cache/weather.json | jq '.main.humidity')}%
]]
