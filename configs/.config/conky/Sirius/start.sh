#!/bin/bash

# This command will close all active conky
killall conky
sleep 2s
		
# Only the config listed below will be avtivated
# if you want to combine with another theme, write the command here
conky -c $HOME/.config/conky/Sirius/Sirius.conf &> /dev/null &
conky -c $HOME/.config/conky/Sirius/Sirius-System.conf &> /dev/null &
conky -c $HOME/.config/conky/Sirius/Sirius-Network.conf &> /dev/null &
conky -c $HOME/.config/conky/Sirius/Sirius-Cpu.conf &> /dev/null &
conky -c $HOME/.config/conky/Sirius/Sirius-Ram.conf &> /dev/null &
conky -c $HOME/.config/conky/Sirius/Sirius-Hds.conf &> /dev/null &
conky -c $HOME/.config/conky/Sirius/Sirius-Meteo.conf &> /dev/null &
conky -c $HOME/.config/conky/Sirius/Sirius-Musicinfo.conf &> /dev/null &

exit
