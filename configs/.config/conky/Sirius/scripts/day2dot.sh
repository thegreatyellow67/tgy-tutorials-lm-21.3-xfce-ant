#!/bin/bash

# Script to display Day as Dot
# Closebox73

DATE=$(date +"%a")

if [[ "${DATE}" = *lun* ]]; then
    echo "      "
elif [[ "${DATE}" = *mar* ]]; then
    echo "      "
elif [[ "${DATE}" = *mer* ]]; then
    echo "      "
elif [[ "${DATE}" = *gio* ]]; then
    echo "      "
elif [[ "${DATE}" = *ven* ]]; then
    echo "      "
elif [[ "${DATE}" = *sab* ]]; then
	echo "      "
elif [[ "${DATE}" = *dom* ]]; then
    echo "      "
else
	echo "Ci sono altri giorni??"
fi

exit
