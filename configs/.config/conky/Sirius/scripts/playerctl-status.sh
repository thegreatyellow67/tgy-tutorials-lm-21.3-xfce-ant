#!/bin/bash

PCTL=$(playerctl status)

if [[ ${PCTL} == "" ]]; then
  echo "MUSICA ASSENTE"
elif [[ ${PCTL} == "Stopped" ]]; then
  echo "RIPRODUZIONE ARRESTATA"
elif [[ ${PCTL} == "Paused" ]]; then
  echo "RIPRODUZIONE IN PAUSA"
else
  echo "STAI ASCOLTANDO:"
fi

exit

