#!/usr/bin/env bash

# Current Theme
dir="$HOME/.config/rofi/menu"
theme='style'

## Esegui
rofi -show drun -icon-theme "McMuse-circle-red" -theme ${dir}/${theme}.rasi

