NOTA: alcuni comandi per installare circle_flow sono nello script install-res.sh. Coloro i quali non utilizzeranno lo script install-res.sh, ma vorranno installare manualmente il tema per Plymouth circle_flow, dovranno utilizzare anche questi comandi:

$ cd ~/Scaricati/lm-21.3-xfce-ant
$ sudo cp -r plymouth-theme/circle_flow/ /usr/share/plymouth/themes/
$ sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/circle_flow/circle_flow.plymouth 100

Eseguire in un terminale i seguenti comandi aggiuntivi:
$ sudo update-alternatives --config default.plymouth (scegliere tema 2)
$ sudo update-initramfs -u

