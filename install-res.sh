#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="ant"
CURSOR_THEME="breezex"
ICON_THEME="mcmuse-circle-red"
THEME_FOLDER="lm-21.3-xfce-${THEME}"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
PROGRESSBAR_SCRIPT="${BASE_PATH}/progress_bar.sh"
PANEL_PROFILE="Ant-Slim"
GRUB_THEME="sugar-candy-ant"
PLYMOUTH_THEME="circle_flow"
#
# ###############################################################
#
# VARIABILI USATE PER INSTALLAZIONE DEI
# PACCHETTI APT E LA BARRA DI PROGRESSO, NON MODIFICARE!
#
# Dichiara una variabile array che contiene i
# pacchetti apt che devono essere installati
declare -a APT_LIST=("bat" "brightnessctl" "btop" "cava" "conky-all" "dconf-editor" "ffmpeg" "fonts-powerline" "fzf" "gimp" "git" "gist" "goodvibes" "gpick" "gnome-calendar" "htop" "inkscape" "jq" "libxfce4ui-utils" "nala" "pinentry-gtk2" "playerctl" "rofi" "ruby" "ruby-dbus" "shutter" "vlc" "xclip" "xfce4-clipman-plugin" "xfce4-panel-profiles" "yad" "zenity")

declare -i ARRAY_COUNT=${#APT_LIST[@]}
declare -i COUNTER=0
declare -i STEP=$(( 100 / ${ARRAY_COUNT} ))
declare -i TOTAL_PROGRESS=100
declare -i PACKAGE_NUM=0
#
# ###############################################################

# ###############################################################
#
# FUNZIONI USATE DALLO SCRIPT, NON MODIFICARE!
#
apt_install() {
 if [ "${COUNTER}" -lt "${ARRAY_COUNT}" ]; then
   echo ""
   echo "  ======"
   echo "   Sto installando il pacchetto: $((PACKAGE_NUM+=1))/${ARRAY_COUNT} - ${APT_LIST[${COUNTER}]}"
   echo "  ======"
   sudo apt install ${APT_LIST[${COUNTER}]} -y &> /dev/null
   sleep 1
   COUNTER=$((COUNTER+1))
 fi
}

progressbar() {
  # Assicurati che la barra di avanzamento venga
  # ripulita quando l'utente preme Ctrl+c
  enable_trapping
  # Crea la barra di progresso
  setup_scroll_area

  for (( PROGRESS=${STEP}; PROGRESS<=${TOTAL_PROGRESS}; PROGRESS+=${STEP} )); do
    apt_install
    draw_progress_bar ${PROGRESS}
  done
  destroy_scroll_area
}
#
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/color-schemes &> /dev/null
fi

if [ ! -f "${PROGRESSBAR_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/progress_bar.sh &> /dev/null
  chmod +x ${BASE_PATH}/progress_bar.sh
fi

source $(dirname $0)/color-schemes
source $(dirname $0)/progress_bar.sh

echo -e "${blue}"
echo ""
echo "  ██╗  ██╗███████╗ ██████╗███████╗     █████╗ ███╗   ██╗████████╗"
echo "  ╚██╗██╔╝██╔════╝██╔════╝██╔════╝    ██╔══██╗████╗  ██║╚══██╔══╝"
echo "   ╚███╔╝ █████╗  ██║     █████╗      ███████║██╔██╗ ██║   ██║"
echo "   ██╔██╗ ██╔══╝  ██║     ██╔══╝      ██╔══██║██║╚██╗██║   ██║"
echo "  ██╔╝ ██╗██║     ╚██████╗███████╗    ██║  ██║██║ ╚████║   ██║"
echo "  ╚═╝  ╚═╝╚═╝      ╚═════╝╚══════╝    ╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝"
echo -e "${red}${bold}"
echo "  =========================================================="
echo "  Script per automatizzare la copia delle"
echo "  risorse per il tema Xfce ${THEME^^}"
echo "  Scritto da TGY-TUTORIALS il 23/04/2024"
echo "  Versione: 1.9"
echo "  Ultima modifica: 25/04/2024"
echo "  =========================================================="
echo -e "${reset}"

function goto
{
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo "  Termino lo script...a presto!"
    exit 1
    ;;
  *)
    echo "  Tasto non valido. Per favore premi 's' o 'n'."
    sleep 5
    exit 1
    ;;
esac

main:

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

if [ ! -d ${THEME_FOLDER} ];then

  echo ""
  echo "  Sto per scaricare le risorse da GitLab, un pò di pazienza..."
  echo ""

  #
  # Installa git se mancante
  #
  if ! location="$(type -p "git")" || [ -z "git" ]; then
    echo "  Installo git per far funzionare questo script..."
    echo ""
    sudo apt install -y git &> /dev/null
  fi

  git clone https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}.git ${THEME_FOLDER}

  cd ${THEME_FOLDER}
  rm -fr .git

  #
  # Per la lista dei pacchetti vedi variabile array APT_LIST
  #
  clear
  echo ""
  echo "  Sto installando alcuni pacchetti utili, un pò di pazienza..."
  echo "  ============================================================================================="
  echo "   Pacchetti:"
  echo "   bat | brightnessctl | btop | cava | conky-all | dconf-editor | ffmpeg | fonts-powerline"
  echo "   fzf | gimp | git | gist | goodvibes | gpick | gnome-calendar | htop | inkscape | jq"
  echo "   libxfce4ui-utils (xfce4-about) | nala | pinentry-gtk2 | playerctl | rofi | ruby | ruby-dbus"
  echo "   shutter | vlc | xclip | xfce4-clipman-plugin | xfce4-panel-profiles | yad | zenity"
  echo ""
  echo "  ATTENZIONE! ATTENZIONE! ATTENZIONE!"
  echo ""
  echo "  NON interrompere lo script in fase di installazione dei pacchetti"
  echo "  apt, quasi sicuramente si corromperà il database di apt!"
  echo "  ============================================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  progressbar

  #
  # Crea cartelle eventualmente mancanti
  #
  if [ ! -d ~/.fonts ];then
    mkdir -p ~/.fonts
  fi

  if [ ! -d ~/.themes ];then
    mkdir -p ~/.themes
  fi

  if [ ! -d ~/.icons ];then
    mkdir -p ~/.icons
  fi

  if [ ! -d ~/.config/conky ];then
    mkdir -p ~/.config/conky
  fi

  if [ ! -d ~/.config/autostart ];then
    mkdir -p ~/.config/autostart
  fi

  if [ ! -d ~/.local/share/applications ];then
    mkdir -p ~/.local/share/applications
  fi

  if [ ! -d /boot/grub/themes ];then
    sudo mkdir -p /boot/grub/themes
  fi

  clear
  echo ""
  echo "  Sostituzione con utente corrente in"
  echo "  alcuni files di configurazione..."
  echo "  ==================================="
  echo ""
  find configs/.config/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs-last/.config/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs/.local/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs/xfce-config-helper/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find user-scripts/.scripts/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +

  clear
  echo ""
  echo "  Importazione della configurazione dei pannelli"
  echo "  tramite xfce4-panel-profiles..."
  echo "  =============================================="
  echo ""
  #
  # Sostituzione con utente corrente nella configurazione per i
  # pannelli (Ant-Slim.tar.bz2) da importare con xfce4-panel-profiles
  #
  mkdir -p configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  tar -xf configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2 -C configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  rm configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2
  find configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  cd configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  tar -cvjSf ${BASE_PATH}/${THEME_FOLDER}/configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2 * &>/dev/null
  cd ${BASE_PATH}/${THEME_FOLDER}
  rm -fr configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  #
  # Importazione della configurazione con xfce4-panel-profiles
  #
  /usr/bin/xfce4-panel-profiles load configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2
  sleep 3

  clear
  echo ""
  echo "  Installazione dei caratteri..."
  echo "  ==================================================================================="
  echo "  Caratteri utilizzati in questo tema:"
  echo ""
  echo "  Hermit Nerd Font:"
  echo "  https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/Hermit.zip"
  echo ""
  echo "  JetBrainsMono Nerd Font (alternativo, per documenti):"
  echo "  https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/JetBrainsMono.zip"
  echo "  ==================================================================================="
  echo ""
  cp -r ${THEME}-fonts/* ~/.fonts
  sudo cp -r ${THEME}-fonts/Hermit/ /usr/share/fonts/truetype/
  sudo cp -r ${THEME}-fonts/JetBrainsMono/ /usr/share/fonts/truetype/
  fc-cache -fr
  sudo fc-cache -fr
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema GTK ${THEME^^}, un pò di pazienza..."
  echo "  =============================================================="
  echo "  Fonti:"
  echo ""
  echo "  https://github.com/EliverLara/Ant"
  echo "  https://www.pling.com/p/1099856/"
  echo "  =============================================================="
  echo ""
  cp -r ${THEME}-gtk-themes/* ~/.themes
  sudo cp -r ${THEME}-gtk-themes/* /usr/share/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone ${ICON_THEME^^}, un pò di pazienza..."
  echo "  =================================================================="
  echo "  Fonti:"
  echo ""
  echo "  https://github.com/yeyushengfan258/McMuse-circle"
  echo "  https://www.xfce-look.org/p/1362227"
  echo "  =================================================================="
  echo ""
  cp -r ${THEME}-icons/* ~/.icons
  sudo cp -r ${THEME}-icons/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione dei cursori ${CURSOR_THEME^^}, un pò di pazienza..."
  echo "  ===================================================================="
  echo "  Fonti:"
  echo ""
  echo "  https://github.com/ful1e5/BreezeX_Cursor/releases"
  echo "  https://www.xfce-look.org/p/1538515"
  echo "  ===================================================================="
  echo ""
  sudo cp -r ${THEME}-cursors/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione degli sfondi..."
  echo "  ============================="
  echo ""
  cp -r ${THEME}-backgrounds/* ~/Immagini
  sleep 3

  clear
  echo ""
  echo "  Installazione di icone varie usate dal tema..."
  echo "  =========================================================================="
  echo "  Elenco:"
  echo ""
  echo "  - Icone personalizzate per i plugins del pannello (create da me)"
  echo "  - Icone per le azioni personalizzate di Thunar (create da me)"
  echo "  - Icone per il plugin del Meteo:"
  echo "    icone clima: https://www.gnome-look.org/p/1165881"
  echo "  - Icone Flattycakes per GIMP"
  echo "    pagina dello sviluppatore: https://github.com/Jantcu/flattycakes"
  echo "  =========================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r misc-icons/.icons/* ~/.icons
  sudo cp -r misc-icons/usr/share/xfce4/weather/icons/* /usr/share/xfce4/weather/icons/ 
  sudo cp -r misc-icons/usr/share/gimp/2.0/icons/* /usr/share/gimp/2.0/icons/

  clear
  echo ""
  echo "  Installazione di configurazioni varie e utilità di ripristino..."
  echo "  =========================================================================="
  echo "  Elenco:"
  echo ""
  echo "  - File per avvio automatico di applicazioni (autostart)"
  echo "  - Script di Conky"
  echo "    tema originale: https://www.pling.com/p/1854716"
  echo "  - Avviatore di applicazioni Plank"
  echo "    tema: https://www.gnome-look.org/p/1911700"
  echo "  - Gestore files Thunar"
  echo "  - Azioni personalizzate di Thunar"
  echo "    https://github.com/cytopia/thunar-custom-actions"
  echo "  - Rofi Menu e PowerMenu"
  echo "  - Visualizzatore audio Cava"
  echo "  - Visualizzatore audio Glava"
  echo "  - macchina"
  echo "  - neofetch"
  echo "  - Impostazioni di Xfce"
  echo "  - Impostazioni del terminale di Xfce"
  echo "    tema: Pencil-Dark (https://gogh-co.github.io/Gogh/)"
  echo "  - Utilità per ripristino impostazioni di Xfce (xfconf-load e xfconf-dump)"
  echo "  - File weather.json per le impostazioni del meteo di Conky"
  echo "  - Personalizzazione di GIMP"
  echo "  - File personalizzato gtk.css (in ~/.config/gtk-3.0)"
  echo "    per una ottimizzazione del tema GTK"
  echo "  - Impostazioni dell'editor di testo (xed)"
  echo "  =========================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r configs/.config/* ~/.config
  cp -r configs/.local/bin/ ~/.local/
  cp -r configs/.local/share/* ~/.local/share/
  cp configs/.cache/weather.json ~/.cache/
  dconf load /net/launchpad/plank/ < configs/plank-settings.conf
  dconf load /org/x/editor/ < configs/xed-settings.conf

  clear
  echo ""
  echo "  Sostituzione del dipositivo di rete WiFi o LAN"
  echo "  nello script di Conky..."
  echo "  ==============================================="
  echo ""

  WIFI_DEVICE=`nmcli device | grep wifi\ | awk '{print $1}'`
  LAN_DEVICE=`ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}'`

  # Sostituzione dei dipositivi di rete WiFi o LAN
  if [[ "${WIFI_DEVICE}" != "" ]]; then
    sed -i "s/wlan0/${WIFI_DEVICE}/g" ~/.config/conky/Sirius/Sirius-Network.conf
  fi

  if [[ "${LAN_DEVICE}" != "" ]]; then
    sed -i "s/enp0s3/${LAN_DEVICE}/g" ~/.config/conky/Sirius/Sirius-Network.conf
  fi
  sleep 3

  clear
  echo ""
  echo "  Installazione di scripts utente personalizzati..."
  echo "  ================================================="
  echo ""
  cp -r user-scripts/.scripts ~/
  sleep 3

  clear
  echo ""
  echo "  Copia di un avatar generico per l'utente..."
  echo "  ======================================================================"
  echo "  NOTA BENE:"
  echo ""
  echo "  nella cartella ${THEME_FOLDER}/resources/avatars/ puoi"
  echo "  trovare differenti icone avatar per rappresentare il tuo utente."
  echo "  Basta copiare quella di tuo gradimento nella cartella:"
  echo ""
  echo "  ${HOME}"
  echo ""
  echo "  e rinominarla in .face"
  echo "  ======================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r resources/avatars/avatar-male-female.svg ~/.face

  clear
  echo ""
  echo "  Installazione di macchina..."
  echo "  ============================"
  echo ""
  sudo cp macchina/macchina-linux-x86_64 /usr/local/bin/macchina
  sudo chmod +x /usr/local/bin/macchina
  sleep 3

  clear
  echo ""
  echo "  Installazione di RadioTray-NG, un pò di pazienza..."
  echo "  ==================================================="
  echo ""
  sudo dpkg -i radiotray-ng/radiotray-ng_0.2.8_ubuntu_22.04_amd64.deb &> /dev/null
  sudo apt install -fy &> /dev/null
  sleep 3

  clear
  echo ""
  echo "  Installazione dello sfondo per la finestra di accesso..."
  echo "  ========================================================"
  echo ""
  sudo cp -r login-window/ /usr/share/backgrounds
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema Sugar Candy Ant per Grub..."
  echo "  =================================================="
  echo ""
  sudo cp -r ${GRUB_THEME}/ /boot/grub/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema ${PLYMOUTH_THEME} per Plymouth..."
  echo "  ========================================================================"
  echo "  Nel terminale si dovranno poi eseguire i seguenti comandi:"
  echo ""
  echo "  $ sudo update-alternatives --config default.plymouth (scegliere tema 2)"
  echo "  $ sudo update-initramfs -u"
  echo "  ========================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  sudo cp -r plymouth-theme/${PLYMOUTH_THEME}/ /usr/share/plymouth/themes/
  sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/${PLYMOUTH_THEME}/${PLYMOUTH_THEME}.plymouth 100
  
  clear
  echo ""
  echo "  Importazione delle impostazioni di Xfce4 tramite xfconf-load..."
  echo "  Fonte: https://github.com/felipec/xfce-config-helper"
  echo "  ================================================================"
  echo "  Per esportare le impostazioni di Xfce4:"
  echo "  xfconf-dump > config.yml"
  echo ""
  echo "  Per importare le impostazioni di Xfce4:"
  echo "  xfconf-load config.yml"
  echo "  ================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  ${HOME}/.local/bin/xfconf-load configs/xfce-config-helper/xfce4-settings.yml &>/dev/null

  clear
  echo ""
  echo "  Risorse installate con successo!"
  echo "  ================================"
  sleep 3
  echo ""
  echo "  ██████╗ ██╗ █████╗ ██╗   ██╗██╗   ██╗██╗ ██████╗"
  echo "  ██╔══██╗██║██╔══██╗██║   ██║██║   ██║██║██╔═══██╗"
  echo "  ██████╔╝██║███████║██║   ██║██║   ██║██║██║   ██║"
  echo "  ██╔══██╗██║██╔══██║╚██╗ ██╔╝╚██╗ ██╔╝██║██║   ██║"
  echo "  ██║  ██║██║██║  ██║ ╚████╔╝  ╚████╔╝ ██║╚██████╔╝"
  echo "  ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝  ╚═══╝    ╚═══╝  ╚═╝ ╚═════╝"
  echo ""
  echo "  ██████╗ ███████╗██╗         ██████╗  ██████╗"
  echo "  ██╔══██╗██╔════╝██║         ██╔══██╗██╔════╝"
  echo "  ██║  ██║█████╗  ██║         ██████╔╝██║"
  echo "  ██║  ██║██╔══╝  ██║         ██╔═══╝ ██║"
  echo "  ██████╔╝███████╗███████╗    ██║     ╚██████╗    ██╗██╗██╗"
  echo "  ╚═════╝ ╚══════╝╚══════╝    ╚═╝      ╚═════╝    ╚═╝╚═╝╚═╝"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  
  #
  # Prima di riavviare, lo script copia la configurazione
  # del terminale di Xfce, perché se lo faccio durante lo script
  # l'aspetto del terminale cambia in modo sgradevole
  #
  cp -r configs-last/.config/xfce4/* ~/.config/xfce4

  sudo systemctl reboot

else
  echo "  La cartella ${THEME_FOLDER} esiste! se vuoi scaricare"
  echo "  le risorse nuovamente devi cancellare la cartella e"
  echo "  rieseguire questo script in un terminale. Buona giornata!"
  echo ""
fi
