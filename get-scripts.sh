#!/bin/bash

THEME="ant"
BASE_PATH="${HOME}/Scaricati"
INSTALLRES_SCRIPT="${BASE_PATH}/install-res.sh"
INSTALLGLAVA_SCRIPT="${BASE_PATH}/install-glava.sh"
INSTALLCOL_SCRIPT="${BASE_PATH}/install-colorscript.sh"
FIXPERMS_SCRIPT="${BASE_PATH}/fix-permissions.sh"

clear
echo ""
echo "  Sto scaricando gli scripts per il tema Ant per te..."
echo "  un attimo di pazienza..."
echo "  ===================================================="
echo ""
#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

if [ ! -f "${INSTALLRES_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/install-res.sh &> /dev/null
  chmod +x ${BASE_PATH}/install-res.sh
  echo "  Ho scaricato lo script per il tema Ant install-res.sh!"
  echo ""
else
  echo "  Lo script: ${INSTALLRES_SCRIPT} è già esistente nel percorso!"
  echo ""
fi

if [ ! -f "${INSTALLGLAVA_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/install-glava.sh &> /dev/null
  chmod +x ${BASE_PATH}/install-glava.sh
  echo "  Ho scaricato lo script per il tema Ant install-glava.sh!"
  echo ""
else
  echo "  Lo script: ${INSTALLGLAVA_SCRIPT} è già esistente nel percorso!"
  echo ""
fi

if [ ! -f "${INSTALLCOL_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/install-colorscript.sh &> /dev/null
  chmod +x ${BASE_PATH}/install-colorscript.sh
  echo "  Ho scaricato lo script per il tema Ant install-colorscript.sh!"
  echo ""
else
  echo "  Lo script: ${INSTALLCOL_SCRIPT} è già esistente nel percorso!"
  echo ""
fi

if [ ! -f "${FIXPERMS_SCRIPT}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/fix-permissions.sh &> /dev/null
  chmod +x ${BASE_PATH}/fix-permissions.sh
  echo "  Ho scaricato lo script per il tema Ant fix-permissions.sh!"
  echo ""
else
  echo "  Lo script: ${FIXPERMS_SCRIPT} è già esistente nel percorso!"
  echo ""
fi

